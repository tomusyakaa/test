<?php

namespace BlogBundle\Controller;

use BlogBundle\Entity\Post;
use FOS\RestBundle\Controller\FOSRestController;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use BlogBundle\Form\PostType;

class ApiController extends FOSRestController
{

    /**
     * @Route("/api/create_post", name="api_post_create")
     */
    public function createPostAction(Request $request)
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post, array('csrf_protection' => false));
        $data = json_decode($request->getContent(), true);
        $form->submit($data);

        $statusCode = 412;

        if($form->isValid()) {
            $user = $this->container->get('security.token_storage')->getToken()->getUser();
            $post->setCreatedAt(new \DateTime());
            $post->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($post);
            $em->flush();

            $statusCode = 201;

            $view = $this->view($post, $statusCode)
                ->setTemplate("BlogBundle:Default:_post.html.twig")
                ->setTemplateVar('posts');
            return $this->handleView($view);
        }

        $view = $this->view($data, $statusCode)->setFormat('json');
        return $this->handleView($view);
    }

    /**
     * @Route("/api/delete_post", name="api_post_delete")
     */
    public function deleteAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $statusCode = 406;
        if ($id = $data['id'])
        {
            $repo = $this->getDoctrine()->getRepository('BlogBundle:Post');
            $post = $repo->find($id);

            $em = $this->getDoctrine()->getEntityManager();
            $em->remove($post);
            $em->flush();
            $statusCode = 200;
        }

        $view = $this->view(null, $statusCode)->setFormat('json');
        return $this->handleView($view);
    }

    /**
     * @Route("/api/more_posts", name="api_posts_more")
     */
    public function redirectAction(Request $request)
    {
        $data = json_decode($request->getContent(), true);

        $statusCode = 406;
        if ($id = $data['id'])
        {
            /* @var $repo \BlogBundle\Repository\PostRepository */
            $repo = $this->getDoctrine()->getRepository('BlogBundle:Post');
            $posts = $repo->getMorePosts($id);
            $statusCode = 201;

            $view = $this->view($posts, $statusCode)
                ->setTemplate("BlogBundle:Default:_post.html.twig")
                ->setTemplateVar('posts');
            return $this->handleView($view);
        }

        $view = $this->view(null, $statusCode)->setFormat('json');
        return $this->handleView($view);
    }
}