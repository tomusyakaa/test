<?php

namespace BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use BlogBundle\Entity\Post;
use BlogBundle\Form\PostType;

class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $user = $this->container->get('security.token_storage')->getToken()->getUser();

        /* @var $repo \BlogBundle\Repository\PostRepository */
        $repo = $this->getDoctrine()->getRepository('BlogBundle:Post');
        $posts = $repo->getAllActivePosts();

        return $this->render('BlogBundle:Default:index.html.twig', array(
            'form' => $form->createView(),
            'posts' => $posts,
            'user' => $user
        ));
    }
}
