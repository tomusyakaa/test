<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
    /**
     * @Route("/profile")
     */
    public function indexAction()
    {
        /* @var \UserBundle\Entity\User $user */
        $user = $this->get('security.token_storage')->getToken()->getUser();

        return $this->render('UserBundle:Default:index.html.twig');
    }
}
